
class RegistrationController < App

	get "/new/:programe" do
		@registration = Registration.new
		@registration.programe = params[:programe]
	  erb :"registrations/new"
	end

	get "/done" do
		@registration = session[:registration]
		erb :'registrations/done'
	end

	get "/", :can => [ :admin, :all ]  do
		@registrations = Registration.all
		erb :'registrations/index'
	end

	get "/:id", :can => [ :admin, :all ]  do
		@registration = Registration.find(params[:id])
		erb :'registrations/show'
	end

	post "/" do
		@registration = Registration.new(params[:registration])
		if @registration.save
			session[:registration] = @registration
		  redirect "registrations/done"  
    else
      erb :'registrations/new'
    end
	end

	

end