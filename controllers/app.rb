require 'sinatra'
require 'sinatra/base'
require 'compass'
require 'sinatra/form_helpers'
require 'sinatra/activerecord'
require 'sinatra/can'
require './config/environments'
Dir.glob("#{Dir.pwd}/models/*.rb").each { |file| require file }


class App < Sinatra::Base
	register Sinatra::Can
	helpers Sinatra::FormHelpers

	configure do
		set :views, "#{Dir.pwd}/views"
		set :root, File.dirname(__FILE__)
		set :scss, {:style => :compact, :debug_info => false}
		Compass.add_project_configuration(File.join(Sinatra::Application.root, '../config', 'compass.config'))
	end

	get '/stylesheets/:name.css' do
	  content_type 'text/css', :charset => 'utf-8'
	  scss(:"stylesheets/#{params[:name]}" )
	end

	def current_user
		params[:user]
	end

  ability do |user|
    can :admin, :all if user == "admin"
  end

	error 403 do
    'not authorized'
  end
end
