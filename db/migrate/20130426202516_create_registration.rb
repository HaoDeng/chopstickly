class CreateRegistration < ActiveRecord::Migration
  def up
  	create_table :registrations do |t|
      t.string :firstname
      t.string :lastname
      t.string :gender
      t.string :email
      t.string :phone
      t.string :edu_bg
      t.string :chinese_level
      t.string :programe
      t.text :others
      t.string :state
      t.timestamps
    end
  end

  def down
  	drop_table :registrations
  end
end
