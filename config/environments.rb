configure :development, :test do
  set :database, "sqlite:///#{Dir.pwd}/db/database.db"
end

# These Heroku setup instructions can be at: https://devcenter.heroku.com/articles/rack
configure :production do
  # Database connection
  db = URI.parse(ENV['DATABASE_URL'] || 'postgres://localhost/mydb')

  ActiveRecord::Base.establish_connection(
    :adapter  => db.scheme == 'postgres' ? 'postgresql' : db.scheme,
    :host     => db.host,
    :username => db.user,
    :password => db.password,
    :database => db.path[1..-1],
    :encoding => 'utf8'
  )
end