require './controllers/app'
Dir.glob('./{helpers,controllers}/*.rb').each { |file| require file }

map('/registrations') { run RegistrationController }
map('/') { run MainController }